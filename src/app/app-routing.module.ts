import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IngresarComponent } from './components/routes/ingresar/ingresar.component';
import { InicioComponent } from './components/routes/inicio/inicio.component';
import { PelisComponent } from './components/routes/pelis/pelis.component';
import { SeriesComponent } from './components/routes/series/series.component';

const routes: Routes = [
{
  path: 'Inicio',
  component: InicioComponent  
},
{
  path: 'Pelis',
  component: PelisComponent
},
{
  path: 'Series',
  component: SeriesComponent
},
{
  path: 'Ingresar',
  component: IngresarComponent
},
{
  path: '**',
  redirectTo: 'Inicio' 
},
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
