import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InicioComponent } from './inicio/inicio.component';
import { SeriesComponent } from './series/series.component';
import { PelisComponent } from './pelis/pelis.component';
import { IngresarComponent } from './ingresar/ingresar.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { SharedModule } from '../shared/shared.module';



@NgModule({
  declarations: [
    InicioComponent,
    SeriesComponent,
    PelisComponent,
    IngresarComponent,

  ],
  imports: [
  CommonModule,
  AppRoutingModule,
  SharedModule
]
})
export class RoutesModule { }
